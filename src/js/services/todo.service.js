angular.module('todo').service('toDoService', ['$http', toDoService]);
function toDoService($http) {
  this.getToDos = function() {
    return [
      {
        title: 'To Do 1'
      },
      {
        title: 'To Do 2'
      }
    ];
  };

  this.getToDosAsync = function() {
    return $http.get('http://localhost:1234/getToDos');
  };
}
