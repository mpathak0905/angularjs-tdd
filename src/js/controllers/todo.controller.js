angular.module('todo').controller('ToDoController', [ToDoController]);
function ToDoController() {
  var VM = this;
  VM.todo = {
    title: ''
  };
  VM.toDoList = [];

  VM.createToDo = createToDo;
  VM.getToDo = getToDo;
  VM.getToDos = getToDos;

  function createToDo(toDoObject) {
    VM.toDoList.push(toDoObject);
  }

  function getToDo(id) {
    return VM.toDoList[id] || undefined;
  }

  function getToDos() {
    return VM.toDoList;
  }
}
