angular.module('todo').directive('toDo', [ToDoDirective]);
function ToDoDirective() {
  return {
    restrict: 'E',
    template:
      '<div><input type="text" placeholder="enter todo title" value="" class="todo-title" /><button type="button" class="btn-add">Add</button></div>',
    scope: {
      onAdd: '=onAdd'
    },
    link: function(scope, element) {
      var parent = element[0];
      var btnAdd = parent.querySelector('.btn-add');
      var titleInput = parent.querySelector('.todo-title');
      if (btnAdd) {
        btnAdd.addEventListener('click', function() {
          var toDoObject = {
            title: titleInput ? titleInput.value : ''
          };
          scope.onAdd(toDoObject);
        });
      }
    }
  };
}
