angular.module('todo').filter('sort', [
  function() {
    return toDoFilter;
  }
]);

function toDoFilter(list, key, order) {
  if (!list) {
    return list;
  }

  list.sort(function(a, b) {
    if (a[key] > b[key]) {
      return order == 'asc' ? 1 : -1;
    } else if (a[key] < b[key]) {
      return order == 'asc' ? -1 : 1;
    } else {
      return 0;
    }
  });

  return list;
}
