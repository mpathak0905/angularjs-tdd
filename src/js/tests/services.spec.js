describe('toDoService', function() {
  var toDoService, httpBackend;
  beforeEach(function() {
    module('todo');
    inject(function($httpBackend, _toDoService_) {
      toDoService = _toDoService_;
      httpBackend = $httpBackend;
    });
  });
  afterEach(function() {
    httpBackend.verifyNoOutstandingExpectation();
    httpBackend.verifyNoOutstandingRequest();
  });

  it('getToDosAsync Test spec', function() {
    var expectedData = [
      {
        title: 'To Do 1'
      },
      {
        title: 'To Do 2'
      }
    ];
    httpBackend
      .expectGET('http://localhost:1234/getToDos')
      .respond(expectedData);

    var promise = toDoService.getToDosAsync();
    var data;
    promise.then(function(res) {
      data = res.data;
    });
    httpBackend.flush();

    expect(expectedData).toEqual(data);
  });
});
