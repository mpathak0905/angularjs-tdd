describe('Filters', function() {
  beforeEach(module('todo'));

  describe('sort', function() {
    var sort;
    beforeEach(inject(function($filter) {
      sort = $filter('sort', {});
    }));

    it('should sort given array in asc order by name', function() {
      var array = [
        { id: 1, name: 'John' },
        { id: 2, name: 'Robert' },
        { id: 3, name: 'Adam' }
      ];
      var expectedSortedArray = [
        { id: 3, name: 'Adam' },
        { id: 1, name: 'John' },
        { id: 2, name: 'Robert' }
      ];

      var sortedArray = sort(array, 'name', 'asc');

      expect(sortedArray).toBeDefined();
      expect(sortedArray).toEqual(expectedSortedArray);
    });

    it('should sort given array in desc order by name', function() {
      var array = [
        { id: 1, name: 'John' },
        { id: 2, name: 'Robert' },
        { id: 3, name: 'Adam' }
      ];
      var expectedSortedArray = [
        { id: 2, name: 'Robert' },
        { id: 1, name: 'John' },
        { id: 3, name: 'Adam' }
      ];

      var sortedArray = sort(array, 'name', 'desc');

      expect(sortedArray).toBeDefined();
      expect(sortedArray).toEqual(expectedSortedArray);
    });
  });
});
