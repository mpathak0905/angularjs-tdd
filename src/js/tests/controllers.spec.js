describe('ToDoController', function() {
  var $controller;

  beforeEach(module('todo'));

  beforeEach(inject(function(_$controller_) {
    $controller = _$controller_;
  }));

  describe('createToDo', function() {
    it('post createToDo(), toDoList length should increase', function() {
      var controller = $controller('ToDoController', {});
      var preAddLength = controller.toDoList.length;
      controller.createToDo({ title: 'To Do 1' });
      expect(controller.toDoList).toBeDefined();
      expect(controller.toDoList.length).toBe(preAddLength + 1);
    });
  });
});
