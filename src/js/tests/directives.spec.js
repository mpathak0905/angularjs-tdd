describe('toDo Directive', function() {
  var compile, scope, directiveElem;

  beforeEach(function() {
    module('todo');

    inject(function($compile, $rootScope) {
      compile = $compile;
      scope = $rootScope.$new();
    });

    directiveElem = getCompiledElement();
  });

  function getCompiledElement() {
    var element = angular.element('<to-do />');
    var compiledElement = compile(element)(scope);
    scope.$digest();
    return compiledElement;
  }

  it('Directive should have input child', function() {
    var wrpaperElement = angular.element(directiveElem.find('div'))[0];
    var titleInput = wrpaperElement.querySelector('.todo-title');
    expect(titleInput).toBeDefined();
  });
});
